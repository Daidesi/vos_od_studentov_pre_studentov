class AddDateToOfferDemands < ActiveRecord::Migration
  def change
    add_column :offer_demands, :date, :datetime
  end
end
