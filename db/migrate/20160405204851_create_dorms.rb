class CreateDorms < ActiveRecord::Migration
  def change
    create_table :dorms do |t|
      t.text :name
      t.string :region

      t.timestamps null: false
    end
  end
end
