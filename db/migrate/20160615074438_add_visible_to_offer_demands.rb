class AddVisibleToOfferDemands < ActiveRecord::Migration
  def change
    add_column :offer_demands, :visible, :boolean
  end
end
