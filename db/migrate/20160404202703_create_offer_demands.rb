class CreateOfferDemands < ActiveRecord::Migration
  def change
    create_table :offer_demands do |t|
      t.text :title
      t.text :content
      t.float :price
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
