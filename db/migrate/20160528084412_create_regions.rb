class CreateRegions < ActiveRecord::Migration
  def change
    create_table :regions do |t|
      t.string :name

      t.timestamps null: false
    end

    remove_column :dorms, :region
    add_reference :dorms, :region, index: true, foreign_key: true
  end
end
