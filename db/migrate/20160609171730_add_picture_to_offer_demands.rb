class AddPictureToOfferDemands < ActiveRecord::Migration
  def change
    add_column :offer_demands, :picture, :string
  end
end
