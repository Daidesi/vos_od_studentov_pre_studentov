class AddTypeToOfferDemands < ActiveRecord::Migration
  def change
    add_reference :offer_demands, :type, index: true, foreign_key: true
  end
end
