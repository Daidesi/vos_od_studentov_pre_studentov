# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Type.create!(name: 'Požičanie')
Type.create!(name: 'Služba')
Type.create!(name: 'Dopyt')
Type.create!(name: 'Udalosť')
Type.create!(name: 'Iné')

bratislava = Region.create!(name: 'Bratislavský kraj')
trnava = Region.create!(name: 'Trnavský kraj')
trencin = Region.create!(name: 'Trenčiansky kraj')
nitra = Region.create!(name: 'Nitriansky kraj')
zilina = Region.create!(name: 'Žilinský kraj')
banskabystica = Region.create!(name: 'Banskobystrický kraj')
presov = Region.create!(name: 'Prešovský kraj')
kosice = Region.create!(name: 'Košický kraj')

bratislava.dorms.create!(name: 'Internát Mladosť')
bratislava.dorms.create!(name: 'Manželské internáty - ŠDaJ Ľ.Štúra')
bratislava.dorms.create!(name: 'Výškové bloky - ŠDaJ Ľ.Štúra')
bratislava.dorms.create!(name: 'Átriové domky - ŠDaJ Ľ.Štúra')
bratislava.dorms.create!(name: 'Internát Družba')
bratislava.dorms.create!(name: 'Internát Hroboňova')
bratislava.dorms.create!(name: 'Internát Horský park')
bratislava.dorms.create!(name: 'Intenát Starohájska 4,8')
bratislava.dorms.create!(name: 'Internát Dolnozemská cesta 1')
bratislava.dorms.create!(name: 'Internát Svoradov')
bratislava.dorms.create!(name: 'Internát - Dobrovičova')
bratislava.dorms.create!(name: 'Študentský Domov Nikosa Belojanisa')
bratislava.dorms.create!(name: 'ŠD J. Hronca a N. Belojanisa - Bernolák')
bratislava.dorms.create!(name: 'Internát Mladá Garda')
bratislava.dorms.create!(name: 'Internát Ekonóm')
bratislava.dorms.create!(name: 'Internát Vlčie Hrdlo')

trnava.dorms.create!(name:'Internát Lomonosovova')
trnava.dorms.create!(name:'Internát Miloša Uhra')
trnava.dorms.create!(name:'Internár Univerzity sv. Cyrila a Metoda')
trnava.dorms.create!(name:'Internát VÚJE')

trencin.dorms.create!(name: 'Internát Záblatie')
trencin.dorms.create!(name: 'Internát Domov mládeže')
trencin.dorms.create!(name: 'Internát Trenčianskej Univerzity A. Dubčeka - Púchov')

nitra.dorms.create!(name: 'Internát Pribina')
nitra.dorms.create!(name: 'Internát Mladosť - Nitra')
nitra.dorms.create!(name: 'Internát UKF - Slančíkovej')
nitra.dorms.create!(name: 'Internát UKF Zobor')
nitra.dorms.create!(name: 'Internát pod Zoborom')
nitra.dorms.create!(name: 'Internát Antona Bernoláka')

zilina.dorms.create!(name: 'Internáty Hliny - ŽU')
zilina.dorms.create!(name: 'Internáty Veľký diel - ŽU')
zilina.dorms.create!(name: 'Internát na Hostihore')
zilina.dorms.create!(name: 'Internát Mladá generácia')

banskabystica.dorms.create!(name: 'Internát Domov mládeže - Banská Štiavnica')
banskabystica.dorms.create!(name: 'Internát Ľudovíra Štúra - Starý internát')
banskabystica.dorms.create!(name: 'Internát Domov mládeže - Zvolen')
banskabystica.dorms.create!(name: 'Internát ŠD 1 - Tajovského')
banskabystica.dorms.create!(name: 'Internát ŠD4 - Tr. SNP')

presov.dorms.create!(name: 'Internát Prešovskej Univerzity - Ul. 17. Novembra')
presov.dorms.create!(name: 'Internát Prešovskej Univerzity - Exnárova')

kosice.dorms.create!(name: 'Internát Domov mládeže')

if Rails.env.development?
  User.create!(name: 'Example User',
               email: 'example@railstutorial.org',
               password: 'foobar',
               password_confirmation: 'foobar',
               admin: true,
               activated: true,
               activated_at: Time.zone.now,
               dorm_id: 1)

  35.times do |n|
    name  = Faker::Name.name
    email = "example-#{n+1}@railstutorial.org"
    password = 'password'
    User.create!(name:  name,
                 email: email,
                 password:              password,
                 password_confirmation: password,
                 activated: true,
                 activated_at: Time.zone.now,
                 dorm_id: rand(3) + 1)

  end

  users = User.order(:created_at).take(6)
  10.times do
    content = Faker::Lorem.sentence(5)
    price = rand(3)
    type = rand(5) + 1
    users.each do |user|
      user.offer_demands.create!(title: Faker::Lorem.sentence(1),
                                 content: content,
                                 price: price,
                                 type_id: type,
                                 visible: true)
    end
  end

  offers = OfferDemand.order(:created_at).take(30)
  5.times do
    offers.each { |offer| offer.comments.create!(rating: rand(6),content: Faker::Lorem.sentence(7), user_id: rand(User.count - 6)+ 7)}
  end
end
