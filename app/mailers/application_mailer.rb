class ApplicationMailer < ActionMailer::Base
  default from: 'noreply@odstudentovprestudentov.com'
  layout 'mailer'
end
