module OfferDemandsHelper

  def price_format(price)
    if price == 0
      'Zadarmo'
    else
      "#{price} €"
    end
  end

  def rating_format(avg)
    if avg.nil?
      '0.0'
    else
      "#{avg}"
    end
  end

end
