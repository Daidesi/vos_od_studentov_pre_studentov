class DormsController < ApplicationController

  def show
    @dorm = Dorm.find(params[:id])
    offer_demands = OfferDemand.my_search_in_concrete_dorm(params[:search], params[:id])
    @offers = offer_demands.paginate(:page => params[:page], :per_page => OfferDemand.per_page)
    @offers_with_date = get_offers_with_date offer_demands
  end

  private

    def get_offers_with_date(offers)
      new_offers = []
      offers.each do |offer|
        new_offers.append(offer) unless offer.date.nil?
      end
      new_offers
    end
end
