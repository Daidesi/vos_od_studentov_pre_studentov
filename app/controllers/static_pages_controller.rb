class StaticPagesController < ApplicationController

  def home
    @offer_demands = OfferDemand.my_search(params[:search], params[:page])
  end

  def help
  end

  def about
  end

  def contact
  end
end
