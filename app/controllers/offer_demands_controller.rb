class OfferDemandsController < ApplicationController
  before_action :logged_in_user, only: [:new, :create, :edit, :update, :destroy]
  before_action :correct_user, only: [:edit, :update, :destroy]
  before_action :visible_for_user, only: :show

  def show
    @comments = @offer_demand.comments.paginate(page: params[:page])
    @comment = current_user.comments.build if logged_in?
  end

  def new
    @offer_demand = OfferDemand.new
  end

  def create
    @offer_demand = current_user.offer_demands.new(offer_params)
    if @offer_demand.save
      flash[:success] = 'Nová ponuka vytvorená!'
      redirect_to @offer_demand
    else
      render 'new'
    end
  end

  def edit
  end

  def update
    if @offer_demand.update_attributes(offer_params)
      flash[:success] = 'Ponuka upravená'
      redirect_to @offer_demand
    else
      render 'edit'
    end
  end

  def destroy
    OfferDemand.find(params[:id]).destroy
    flash[:success] = 'Ponuka zmazaná'
    redirect_to current_user
  end

  private

    def offer_params
      params.require(:offer_demand).permit(:title, :content, :price, :date, :picture, :type_id, :visible)
    end

    def correct_user
      @offer_demand = OfferDemand.find(params[:id])
      redirect_to(root_url) unless @offer_demand.user == current_user
    end

    def visible_for_user
      @offer_demand = OfferDemand.my_concrete_offer(params[:id])
      redirect_to(root_url) unless @offer_demand.user == current_user || @offer_demand.visible == true
    end
end
