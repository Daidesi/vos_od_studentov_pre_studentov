class UsersController < ApplicationController
  before_action :logged_in_user, only: [:index, :edit, :update, :destroy]
  before_action :correct_user,   only: [:edit, :update]
  before_action :admin_user,     only: [:index, :destroy]

  def index
    @users = User.paginate(page: params[:page])
  end

  def show
    @user = User.includes(dorm: :region).find(params[:id])
    @offer_demands = OfferDemand.my_all_offers_by_user(params[:id], params[:page])
  end

  def new
    @user = User.new
    @grouped_options = @dorm_with_region.inject({}) do |options, dorm|
      (options[dorm.region.name] ||= []) << [dorm.name, dorm.id]
      options
    end
  end

  def create
    @user = User.new(user_params)
    if @user.save
      @user.send_activation_email
      flash[:info] = 'Please check your email to activate your account.'
      redirect_to root_url
    else
      @grouped_options = @dorm_with_region.inject({}) do |options, dorm|
        (options[dorm.region.name] ||= []) << [dorm.name, dorm.id]
        options
      end
      render 'new'
    end
  end

  def edit
    @grouped_options = @dorm_with_region.inject({}) do |options, dorm|
      (options[dorm.region.name] ||= []) << [dorm.name, dorm.id]
      options
    end
  end

  def update
    if @user.update_attributes(user_params)
      flash[:success] = 'Profile updated'
      redirect_to @user
    else
      @grouped_options = @dorm_with_region.inject({}) do |options, dorm|
        (options[dorm.region.name] ||= []) << [dorm.name, dorm.id]
        options
      end
      render 'edit'
    end
  end

  def destroy
    User.find(params[:id]).destroy
    flash[:success] = 'User deleted'
    redirect_to users_url
  end

  private

    def user_params
      params.require(:user).permit(:name, :email, :password, :password_confirmation, :phone, :dorm_id)
    end

    #Before filters

    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = 'Please log in'
        redirect_to login_url
      end
    end

    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless @user == current_user
    end

    # Confirms an admin user.
    def admin_user
      redirect_to(root_url) unless current_user.admin?
    end

end
