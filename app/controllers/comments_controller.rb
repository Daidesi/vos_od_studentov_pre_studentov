class CommentsController < ApplicationController
  before_action :logged_in_user
  before_action :correct_user, only: :destroy

  def create
    @comment = current_user.comments.build(comment_params)
    if @comment.save
      flash[:success] = 'Komentár vytvorený!'
      redirect_to offer_demand_path(@comment.offer_demand)
    else
      @offer_demand = OfferDemand.my_concrete_offer(@comment.offer_demand.id)
      @comments = @offer_demand.comments.paginate(page: params[:page])
      params[:id] = @comment.offer_demand.id
      render 'offer_demands/show'
    end
  end

  def destroy
    @comment.destroy
    flash[:success] = 'Komentár bol úspešne vymazaný'
    redirect_to request.referrer
  end

  private

    def comment_params
      params.require(:comment).permit(:rating, :content, :offer_demand_id)
    end

    def correct_user
      @comment = current_user.comments.find_by(id: params[:id])
      redirect_to root_url if @comment.nil?
    end
end
