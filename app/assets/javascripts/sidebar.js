/**
 * Created by andrej on 28.5.2016.
 */
$(document).on('ready page:load', function () {

    $('[data-toggle="offcanvas"]').click(function (e) {
        e.preventDefault();
        $('#wrapper').toggleClass('toggled');
    });
});