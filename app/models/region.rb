class Region < ActiveRecord::Base
  has_many :dorms, :foreign_key => 'regions_id'
end
