class Type < ActiveRecord::Base
  has_many :offer_demands
  validates :name, presence: true, length: { maximum: 50 }

end
