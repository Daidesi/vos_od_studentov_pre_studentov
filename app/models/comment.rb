class Comment < ActiveRecord::Base
  belongs_to :user
  default_scope -> { order(created_at: :desc) }
  belongs_to :offer_demand

  validates :rating, numericality: {greater_than_or_equal_to: 0, less_than_or_equal_to: 5}, allow_nil: true
  validates :content, presence: true, length: { maximum: 200 }
  validates :user_id, presence: true
  validates :offer_demand_id, presence: true
  validate  :cant_rate_own_offers

  private

    def cant_rate_own_offers
      unless offer_demand.user_id != user_id || rating.nil?
        errors.add(:user, 'cant rate own offers')
      end
    end

end
