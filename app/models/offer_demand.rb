class OfferDemand < ActiveRecord::Base
  belongs_to :user
  belongs_to :type
  has_many :comments, dependent: :destroy
  mount_uploader :picture, PictureUploader
  validates :user_id, presence: true
  validates :title, presence: true, length: { maximum: 70 }
  validates :content, presence: true, length: { maximum: 200 }
  validates :price, presence: true, numericality: true
  validates :type_id, presence: true
  validate  :picture_size

  def self.per_page
    8
  end

  def start_time
    self.date
  end

  def self.my_search(search, page)
    if search.blank?
      paginate_by_sql(<<-SQL, page: page, per_page: per_page)
        SELECT od.*, round(cast(AVG(com.rating) as numeric),2) as rating,
             t.name as type_name, d.name as dorm_name, r.name as region_name
        FROM offer_demands od
        JOIN users u ON u.id = od.user_id
        JOIN dorms d ON u.dorm_id = d.id
        JOIN regions r ON d.regions_id = r.id
        JOIN types t ON t.id = od.type_id
        LEFT JOIN comments com ON com.offer_demand_id = od.id
        WHERE od.visible = true
        GROUP BY od.id, t.name, d.name, r.name
        ORDER BY od.title
      SQL
    else
      paginate_by_sql(<<-SQL, page: page, per_page: per_page)
        SELECT od.*, round(cast(AVG(com.rating) as numeric),2) as rating,
             t.name as type_name, d.name as dorm_name, r.name as region_name
        FROM offer_demands od
        JOIN users u ON u.id = od.user_id
        JOIN dorms d ON u.dorm_id = d.id
        JOIN regions r ON d.regions_id = r.id
        JOIN types t ON t.id = od.type_id
        LEFT JOIN comments com ON com.offer_demand_id = od.id
        WHERE od.visible = true
        AND (to_tsvector('simple', od.title) @@ plainto_tsquery('simple', #{ActiveRecord::Base.sanitize(search)})
        OR to_tsvector('simple', od.content) @@ plainto_tsquery('simple', #{ActiveRecord::Base.sanitize(search)}))
        GROUP BY od.id, t.name, d.name, r.name
        ORDER BY od.title
      SQL
    end
  end

  def self.my_search_in_concrete_dorm(search, id)
    if search.blank?
      find_by_sql <<-SQL
        SELECT od.*, round(cast(AVG(com.rating) as numeric),2) as rating,
             t.name as type_name, d.name as dorm_name, r.name as region_name
        FROM dorms d
        JOIN regions r ON d.regions_id = r.id
        JOIN users u on u.dorm_id = d.id
        JOIN offer_demands od ON od.user_id = u.id
        JOIN types t ON t.id = od.type_id
        LEFT JOIN comments com ON com.offer_demand_id = od.id
        WHERE od.visible = true AND d.id = #{ActiveRecord::Base.sanitize(id)}
        GROUP BY od.id, t.name, d.name, r.name
        ORDER BY od.title
      SQL
    else
      find_by_sql <<-SQL
        SELECT od.*, round(cast(AVG(com.rating) as numeric),2) as rating,
             t.name as type_name, d.name as dorm_name, r.name as region_name
        FROM dorms d
        JOIN regions r ON d.regions_id = r.id
        JOIN users u on u.dorm_id = d.id
        JOIN offer_demands od ON od.user_id = u.id
        JOIN types t ON t.id = od.type_id
        LEFT JOIN comments com ON com.offer_demand_id = od.id
        WHERE od.visible = true AND d.id = #{ActiveRecord::Base.sanitize(id)}
        AND (to_tsvector('simple', od.title) @@ plainto_tsquery('simple', #{ActiveRecord::Base.sanitize(search)})
        OR to_tsvector('simple', od.content) @@ plainto_tsquery('simple', #{ActiveRecord::Base.sanitize(search)}))
        GROUP BY od.id, t.name, d.name, r.name
        ORDER BY od.title
      SQL
    end
  end

  def self.my_all_offers_by_user(id, page)
    my_sql = <<-SQL
      SELECT od.*, round(cast(AVG(com.rating) as numeric),2) as rating,
             t.name as type_name
      FROM offer_demands od
      LEFT JOIN comments com ON com.offer_demand_id = od.id
      JOIN types t ON t.id = od.type_id
      WHERE od.user_id = #{ActiveRecord::Base.sanitize(id)}
      GROUP BY od.id, t.name
      ORDER BY od.title
    SQL
    paginate_by_sql(my_sql, page: page, per_page: per_page)
  end

  def self.my_count_offers_by_user(user)
    my_sql = <<-SQL
      SELECT count(od.id)
      FROM offer_demands od
      JOIN users u ON u.id = od.user_id
      WHERE u.id = #{ActiveRecord::Base.sanitize(user.id)}
    SQL
    count_by_sql(my_sql)
  end

  def self.my_concrete_offer(id)
    my_sql = <<-SQL
      SELECT od.*, round(cast(AVG(com.rating) as numeric),2) as rating,
             t.name as type_name, d.name as dorm_name, r.name as region_name
      FROM offer_demands od
      JOIN types t ON t.id = od.type_id
      JOIN users u ON u.id = od.user_id
      JOIN dorms d ON u.dorm_id = d.id
      JOIN regions r ON d.regions_id = r.id
      LEFT JOIN comments com ON com.offer_demand_id = od.id
      WHERE od.id = #{ActiveRecord::Base.sanitize(id)}
      GROUP BY od.id, t.name, d.name, r.name
    SQL
    find_by_sql(my_sql)[0]
  end

  private

    # Validates the size of an uploaded picture.
    def picture_size
      if picture.size > 5.megabytes
        errors.add(:picture, 'should be less than 5MB')
      end
    end
end
