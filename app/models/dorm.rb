class Dorm < ActiveRecord::Base
  has_many :users
  belongs_to :region, :foreign_key => 'regions_id'

end
