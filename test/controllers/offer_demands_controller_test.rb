require 'test_helper'

class OfferDemandsControllerTest < ActionController::TestCase

  def setup
    @user = users(:lana)
    @offer = offer_demands(:one)
  end

  test 'should redirect create when not logged in' do
    assert_no_difference 'OfferDemand.count' do
      post :create, offer_demand: { title: 'example',
                                    content: 'Lorem ipsum',
                                    price: 3,
                                    user_id: @user.id}
    end
    assert_redirected_to login_url
  end

  test 'should redirect destroy when not logged in' do
    assert_no_difference 'OfferDemand.count' do
      delete :destroy, id: @offer
    end
    assert_redirected_to login_url
  end
end
