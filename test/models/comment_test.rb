require 'test_helper'

class CommentTest < ActiveSupport::TestCase

  def setup
    @user = users(:michael)
    @my_offer_demand = offer_demands(:one)
    @other_offer_demand = offer_demands(:two)
    @comment = @user.comments.build(rating: 2.5, content: 'Example', offer_demand_id: @other_offer_demand.id)
  end

  test 'order should be most recent first' do
    assert_equal comments(:most_recent), Comment.first
  end

  test 'rating should be from 0 to 5' do
    assert @comment.valid?
    @comment.rating = -3
    assert_not @comment.valid?
    @comment.rating = 7
    assert_not @comment.valid?
  end

  test 'content should be present' do
    @comment.content = '   '
    assert_not @comment.valid?
  end

  test 'content should be at most 200 characters' do
    @comment.content = 'a' * 201
    assert_not @comment.valid?
  end

  test 'user should not be able to rate his own offers' do
    @comment.offer_demand_id = @my_offer_demand.id
    @comment.rating = nil
    assert @comment.valid?
    @comment.rating = 5
    assert_not @comment.valid?
  end

end
