require 'test_helper'

class OfferDemandTest < ActiveSupport::TestCase

  def setup
    @user = users(:michael)
    @offer = @user.offer_demands.build(title: 'Lorem ipsum', content: 'Lorem ipsum', price: 2, type_id: 1, visible: true)
  end

  test 'should be valid' do
    assert @offer.valid?
  end

  test 'user id should be present' do
    @offer.user = nil
    assert_not @offer.valid?
  end

  test 'title should be present' do
    @offer.title = '   '
    assert_not @offer.valid?
  end

  test 'content should be present' do
    @offer.content = '   '
    assert_not @offer.valid?
  end

  test 'associated comments should be destroyed' do
    @offer.save
    @offer.comments.create!(content: 'Lorem ipsum', user_id: @user.id)
    assert_difference 'Comment.count', -1 do
      @offer.destroy
    end
  end

end
