require 'test_helper'

class OfferDemandsShowTest < ActionDispatch::IntegrationTest

  def setup
    @owner = users(:michael)
    @own_offer = offer_demands(:one)
    @other_offer = offer_demands(:two)
    @own_offer.visible = false
    @other_offer.visible = false
  end

  test 'invisible offer visitable only by author' do
    log_in_as(@owner)
    get offer_demand_path(@own_offer)
    assert_template 'offer_demands/show'
    get offer_demand_path(@other_offer)
    follow_redirect!
    assert_template 'static_pages/home'
  end

end
