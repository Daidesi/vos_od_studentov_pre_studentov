# Od studentov pre studentov (from students for students)

This is a web application designed for students.

Application is created as school project for subjects VOS and DBS.

Application created using [*Ruby on Rails Tutorial:
Learn Web Development with Rails*](http://www.railstutorial.org/)
by [Michael Hartl](http://www.michaelhartl.com/).

